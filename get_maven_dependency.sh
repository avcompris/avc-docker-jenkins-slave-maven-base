#!/bin/bash

# File: avc-docker-jenkins-slave-maven-base/get_maven_dependency.sh

set -e

dependency="${1}"

if [ -z "${dependency}" ]; then
	echo "First argument should be dependency, in the form: <groupId>:<artifactId>:<version>" >&2
	echo "Exiting." >&2
	exit 1
fi

mvn -U org.apache.maven.plugins:maven-dependency-plugin:3.1.1:get \
    -Dtransitive=true \
    "-Dartifact=${dependency}"
    
mvn -U org.apache.maven.plugins:maven-dependency-plugin:3.1.1:get \
    -Dtransitive=true \
    "-Dartifact=${dependency}:pom"

