# avc-docker-jenkins-slave-maven-base

Docker image: avcompris/jenkins-slave-maven:base

Usage:

	$ docker run \
		-e JENKINS_PUB_KEY="ssh-rsa AAAAB3Nz...NLsug/a7" \
		avcompris/jenkins-slave-maven:base

Exposed port is 22.
