# File: avc-docker-jenkins-slave-maven-base/Dockerfile
#
# Use to build the image: avcompris/jenkins-slave-maven:base

FROM avcompris/jenkins-slave-ssh
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   1. DEBIAN PACKAGES
#-------------------------------------------------------------------------------

RUN apt-get update

RUN apt-get install -y \
	maven=3.3.9-4

#-------------------------------------------------------------------------------
#   2. ENVIRONMENT
#-------------------------------------------------------------------------------

ENV MAVEN_HOME /usr/share/maven

#-------------------------------------------------------------------------------
#   3. USERS
#-------------------------------------------------------------------------------

USER jenkins
WORKDIR /home/jenkins

RUN mkdir .m2/

#-------------------------------------------------------------------------------
#   4. PRE-FETCHED MAVEN DEPENDENCIES
#-------------------------------------------------------------------------------

COPY get_maven_dependency.sh .

# 4.1. Maven plugins

RUN ./get_maven_dependency.sh org.apache.maven.plugins:maven-clean-plugin:3.1.0
RUN ./get_maven_dependency.sh org.apache.maven.plugins:maven-resources-plugin:3.1.0
RUN ./get_maven_dependency.sh org.apache.maven.plugins:maven-compiler-plugin:3.8.1
RUN ./get_maven_dependency.sh org.apache.maven.plugins:maven-install-plugin:3.0.0-M1
RUN ./get_maven_dependency.sh org.apache.maven.plugins:maven-jar-plugin:3.2.0
RUN ./get_maven_dependency.sh org.apache.maven.plugins:maven-surefire-plugin:3.0.0-M4

# 4.2. Maven dependencies

RUN ./get_maven_dependency.sh org.apache.maven.surefire:surefire-junit-platform:3.0.0-M4

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

USER root
WORKDIR /



